'''
Part of my Python documentation project.
Topics
    split
    join
    string splice
    print formatted
'''


def split_join():
    '''
    Topic: split, join, string, list
    '''
    print('Topic: split and join')
    alist = ['My', 'python', 'documentation', 'project']
    sentence = "A sentence is here"
    csv = "one,two,three,four"

    # Combine list into a string
    ''.join(alist)
    # Combine list separated with a space into a string
    ' '.join(alist)
    # Combine list separated with a comma into a string
    ','.join(alist)

    # Split string into a list
    sentence.split()
    # Split a comma separated file into a list
    csv.split(',')


def string_splice():
    '''
    Topic: string splicing
    '''
    sentence = "A sentence is here"

    # Reverse string
    sentence[::-1]
    # Last three of string
    sentence[-3::]
    # Print ever other letter
    sentence[::2]


def print_formatted():
    '''
    Topic: print f'string'
    '''
    mystring = 'welcome'

    print(f'Hello and {mystring}.')
    print(f'Hello and {mystring.title()}.')


if __name__ == '__main__':
    # Execute when module is not initialized from import statement
    #split_join()
    #string_splice()
    print_formatted()


